import './NavBarColab.css'
import {NavLink} from 'react-router-dom'

const NavBarColab = () => {
  return (
    <nav className='nav-navbar'>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/meu-panorama">Meu Panorama Geral</NavLink><hr/>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/solicitacao-colab">Solicitação de Férias</NavLink><hr/>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/status-colab">Status Solicitações</NavLink>
    </nav>
  )
}

export default NavBarColab;