import React from 'react'
import logoQuero from '../../img/logo_quero.png'
import logoVerde from '../../img/logo_verde.jpg'
import outIcon from '../../img/out-icon.png'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import NotificationsIcon from '@mui/icons-material/Notifications';
import LogoutIcon from '@mui/icons-material/Logout';
import './HeaderGeral.css'
import { Link } from 'react-router-dom';
import { getToken, logout } from '../../services/auth';
import api from '../../services/api';

const Header = (props) => {

  async function confirmSair(){
      const response = await api.get('/funcionarios/destroytoken', {headers:{token: getToken()}})
      if(response.status === 200){
        logout();
        window.location.href = '/'
      } else {
        alert('Não foi possível fazer o logout!')
      }
  }



  return (
    <nav className='nav-header'>
        <div className="container-img">
            <img src={logoQuero} alt="Quero-Quero" className="logo"/>
            <img src={logoVerde} alt="Verde-card" className="logo"/>
        </div>
        <div className='principal-title'>
            <h2>{props.name}</h2>
        </div>
        <div className="container-img">
          <div className="container-mui">
            <NotificationsIcon fontSize='medium' sx={{color: 'white', cursor: 'pointer'}}  />
          </div>
          <div className="container-mui">
            <Link to='/meu-perfil-gestor/:id'><AccountCircleIcon fontSize='large' sx={{color: 'white', cursor: 'pointer'}} /></Link>
          </div>
          <div className="container-mui">
            <Link to='/'><LogoutIcon fontSize='medium' sx={{color: 'white', cursor: 'pointer'}} onClick={confirmSair}/></Link>
          </div>
        </div>
        
    </nav>
  )
};

export default Header;