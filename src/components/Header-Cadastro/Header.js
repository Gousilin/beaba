import React from 'react'
import logoQuero from '../../img/logo_quero.png'
import logoVerde from '../../img/logo_verde.jpg'
import './Header.css'
import {Link} from 'react-router-dom'


const Header = () => {
  return (
    <nav className='nav-header'>
        <div className="container-img">
            <img src={logoQuero} alt="Quero-Quero" className="logo"/>
            <img src={logoVerde} alt="Verde-card" className="logo"/>
        </div>
        <div className='principal-title'>
            <h2>Cadastro de funcionários</h2>
        </div>
        <div className="container-img">
            <Link to="/" className='link-sair'>Sair</Link>
        </div>
        
    </nav>
  )
};

export default Header;