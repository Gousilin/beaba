import React from 'react'
import './NavBarLateral.css'
import {NavLink} from 'react-router-dom'

const NavBarLateral = () => {
  return (
    <div className='nav-lateral'>
        <NavLink className={({ isActive }) => (isActive ? "active-lateral" : undefined)} to="/minhas-gestor/solicitacao"><p>Solicitação de Férias</p></NavLink>
        <NavLink className={({ isActive }) => (isActive ? "active-lateral" : undefined)} to="/minhas-gestor/status"><p>Status Solicitações</p></NavLink>
    </div>
  )
}

export default NavBarLateral