import './NavBarGestor.css'
import {NavLink} from 'react-router-dom'

const NavBarGestor = () => {
  return (
    <nav className='nav-navbar'>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/dashboard-gestor">Dashboard</NavLink><hr/>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/todas-solicitacoes">Solicitações de Férias</NavLink><hr/>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/pesquisa-colaboradores">Colaboradores</NavLink><hr/>
        <NavLink className={({ isActive }) => (isActive ? "active-nav" : undefined)} to="/minhas-gestor/solicitacao">Minhas Solicitações</NavLink>
    </nav>
  )
}

export default NavBarGestor;