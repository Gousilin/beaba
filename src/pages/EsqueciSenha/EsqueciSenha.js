import React from 'react'
import logoQuero from '../../img/logo_quero.png'
import logoVerde from '../../img/logo_verde.jpg'
import {Link} from 'react-router-dom'
import Modal from 'react-modal'
import { useState } from 'react'
import './EsqueciSenha.css'

Modal.setAppElement('#root')


export const EsqueciSenha = () => {
    const [modalIsOpen, setIsOpen] = useState(false)

function handleOpenModal(){
    setIsOpen(true)
}

function handleCloseModal(){
    setIsOpen(false)
}


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        display: 'flex',
        width: '450px',
        borderRadius: '12px'
    }
}


  return (
    <body className='login-body'>
        <div className='box-verde-logo'>
            <img src={logoVerde} alt="Verde-card" className="verde-logo"/>
        </div>
        <div className='container-geral'>
            <fieldset className="container-login">
                <legend className="borda-container-login"><img src={logoQuero} alt="Quero-Quero" className="quero-logo"/></legend>
                <div className='caixa-dados'>
                    <h2>Alterar senha:</h2>
                    <div className="input-login">
                        <label for="login">Login:</label>
                        <input id="login" type="text" name="login" placeholder="Digite seu login..." required/>
                    </div>
                    <div className="botao-restaurar">
                        <button onClick={handleOpenModal} className='texto-botao'>Alterar</button>
                    </div>
                    <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal} style={customStyles}>
                        <div className='style-modal'>
                            <p>Sua senha foi restaurada para a senha padrão disponibilizada pelo RH!</p>
                        </div>
                       
                    </Modal>
                    <div className="botoes-login">
                        <Link to="/" className='texto-redefinir'>Retornar à página de Login</Link>
                    </div>

                </div>
            </fieldset>
        </div> 


    </body>
  )
};

export default EsqueciSenha;