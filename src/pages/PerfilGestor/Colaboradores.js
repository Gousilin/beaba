import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import { Link } from 'react-router-dom'
import './Colaboradores.css'
import { useState, useEffect} from 'react'
import api from '../../services/api'

const Colaboradores = () => {

    const [colaboradores, setColaboradores] = useState([])

    useEffect(() => {
      async function loadColaboradores(){
        const response = await api.get('/listFuncionarios')

        console.log(response.data)
        setColaboradores(response.data)
      }

      loadColaboradores();
    }, [])

    const [pesquisa, setPesquisa] = useState('');
    const pesquisaLowerCase = pesquisa.toLowerCase()
    
    const resultado = colaboradores.filter((row) => row.nome.toLowerCase().includes(pesquisaLowerCase))

    

  return (
    <div>
        <HeaderGestor name="Painel do Gestor"/>
        <NavBarGestor/>
        <body className="bd-soli-gestor">
                <fieldset
                    className="container-principal">
                    <legend className="legenda-borda">Colaboradores da sua equipe</legend>
                    <form className='pesquisa'>
                      <div className="box-pesquisa">
                        <input type="text" placeholder='Pesquise um colaborador' className='entrada-pesquisa' value={pesquisa} onChange={(e) => setPesquisa(e.target.value)}/>
                      </div>
                    </form>
                    <div>
                      {resultado.map((row) => (
                        <div className="container-list" key={row._id}>
                          <p key={row.nome}>{row.nome}</p>
                          <p>Matrícula: {row.id_matricula}</p>
                          <p>Data Contratação: {new Date(row.data_contrato).toLocaleDateString('pt-br')}</p>
                          <Link to={`/pesquisa-colaboradores/colaborador/${row.id_matricula}`} className='botao-visualizar'>Visualizar</Link>
                        </div> 
                      ))}
                      
                    </div>
                                   
                
                
                </fieldset>
            
        </body>
    </div>
  )
}

export default Colaboradores