import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import Calendario from '../../components/Calendario/Calendario.css'
import Calendar from 'react-calendar'
import '../../components/Calendario/Calendario.css'
import { Link } from 'react-router-dom'
import { Scheduler } from "@aldabil/react-scheduler";
import { useState, useEffect} from 'react'
import api from '../../services/api'

const Dashboard = () => {

    const [agenda, setAgenda] = useState([])
    

    useEffect(() => {
      async function loadAgenda(){
        const aprove = await api.get('/listSolicitacaoA')
        setAgenda(aprove.data)
      }
      loadAgenda();

    }, [])


    const agendamentos = [...agenda.map((row) => (
      {
        event_id: row.id_solicitacao,
        title: row.funcionario.nome,
        start: new Date(row.data_inicio).toISOString().slice(0,10),
        end: new Date(row.data_final).toISOString().slice(0,10),
      }))]

      console.log (...agendamentos)


  return (
    <div>
    <HeaderGestor name="Painel do Gestor"/>
    <NavBarGestor/>
    <div className="bd-soli-gestor">
            <div>
            

            </div>
            <fieldset
                className="container-principal">
                <legend className="legenda-borda">Panorama Geral dos Funcionários</legend>
                <Scheduler
                  view="month"
                  events = {agendamentos}
                />
            
            </fieldset>
        
    </div>
</div>
  )
}

export default Dashboard