import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import NavBarLateral from '../../components/NavBarGestor/NavBarLateral'
import { Link } from 'react-router-dom'

const VerStatusGestor = () => {
  return (
    <div>
        <HeaderGestor name="Painel do Gestor"/>
        <NavBarGestor/>
        <body className="bd-my-gestor">
            <NavBarLateral/>
            <div className='container-my'>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Preencha o formulário abaixo para solicitar o período de férias</legend>
                    <div className='status'>
                        <p className='status-analise'>Em análise</p>
                    </div>
                    <div className="container-itens">
                        <div className="input-box">
                            <label for="nome">Nome Completo:</label>
                            <input id="nome" type="text" name="nome" placeholder="Digite seu nome completo" disabled/>
                        </div>
                        <div className="input-box">
                            <label for="matricula">Matrícula:</label>
                            <input id="matricula" type="number" name="matricula" placeholder="Informe a matrícula" disabled/>
                        </div>
                        <div className="input-box">
                            <label for="data-solicita">Data de Solicitação:</label>
                            <input id="data-solicita" type="date" name="data-solicita"disabled/>
                        </div>
                        <div className="input-box">
                            <label for="dias">Quantidade de Dias:</label>
                            <input id="dias" type="text" name="dias" placeholder="Selecione a squad" list="squads" disabled/>
                            <datalist id="dias">
                                <option>5</option>
                                <option>10</option>
                                <option>15</option>
                                <option>20</option>
                                <option>30</option>
                            </datalist>
                        </div>
                        <div className="input-box">
                            <label for="inicio">Início das Férias:</label>
                            <input id="inicio" type="date" name="inicio" disabled/>
                        </div>
                        <div className="input-box">
                            <label for="final">Final das Férias:</label>
                            <input id="final" type="date" name="final" disabled/>
                        </div>
                        <div className="input-box">
                            <label for="final">Comentário ao Gestor:</label>
                            <input id="comentario" type="text" name="comentario" placeholder="Deixe um comentário para seu gestor..." disabled/>
                        </div>
                    </div>
                    <div className="container-itens-treze">
                        <div className="box-treze">
                        <label>Deseja solicitar o adiantamento do 13º?</label>
                            <label for="adianta">Sim</label>
                        </div>
                
                    </div>            
                
                </fieldset>
            </div>
        </body>
    </div>
  )
}

export default VerStatusGestor