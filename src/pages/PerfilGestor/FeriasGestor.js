import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import NavBarLateral from '../../components/NavBarGestor/NavBarLateral'
import './FeriasGestor.css'
import Modal from 'react-modal'
import { useState } from 'react'
import api from '../../services/api'

Modal.setAppElement('#root')

const FeriasGestor = () => {
    //solicitar
    const [dataInicio, setDataInicio] = useState('')
    const [dataFim, setDataFim] = useState('')
    const [duracao, setDuracao] = useState('')
    const [mensagemFuncionario, setMensagemFuncionario] = useState('')
    const [idFuncionario, setIdFuncionario] = useState('')


    async function handleSubmit(){

        const data = {
            data_solicitacao: new Date(),
            data_inicio: dataInicio,
            data_final: dataDoFim(dataInicio, duracao),
            duracao: duracao,
            status: "Análise",
            mensagem_funcionario: mensagemFuncionario,
            idFuncionario: idFuncionario, 
        }

        var checkbox = document.getElementById("adiantamento")

        if (checkbox.checked){
            const dataAdianta = {
                idFuncionario: idFuncionario
            }

            const response = await api.post('/saveAdiantamento', dataAdianta)
        
            console.log(dataAdianta)
            console.log(response)
        }
        

        if (data.data_inicio === "" || data.data_final === "" || data.duracao === "" || data.status === "" || data.idFuncionario === ""){
            alert("Por favor, preencha todos os campos!")
        } else{
            const response = await api.post('/saveSolicitacao', data)
        
            console.log(data)
            console.log(response)

            handleOpenModal()
        }

    }

    //data fim
    function dataDoFim(dataDoInicio, duracaoDias){
        const dataFim = new Date(dataDoInicio)
        const addDias = Number(duracaoDias)

        dataFim.setDate(dataFim.getDate() + addDias)
        return dataFim
        
    }

    //modals
    
    const [modalIsOpen, setIsOpen] = useState(false)

    function handleOpenModal(){
        setIsOpen(true)
    }

    function handleCloseModal(){
        setIsOpen(false)
        window.location.href='/minhas-gestor/status'
    }


    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            width: '330px',
            borderRadius: '12px'
        }
    }

  return (
    <div>
        <HeaderGestor name="Painel do Gestor"/>
        <NavBarGestor/>
        <div className="bd-my-gestor">
            <NavBarLateral/>
            <div className='container-my'>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Preencha o formulário abaixo para solicitar o período de férias</legend>
                    <div className="container-itens">
                        <div className="input-box">
                            <label htmlFor="matricula">Matrícula:</label>
                            <input id="matricula" type="text" name="matricula" placeholder="Informe a matrícula" value={idFuncionario} onChange={e => setIdFuncionario(e.target.value)}required/>
                        </div>
                        <div className="input-box">
                            <label htmlFor="dia">Quantidade de Dias:</label>
                            <input id="dia" type="text" name="dia" placeholder="Selecione a duração" list="dias" value={duracao} onChange={e => setDuracao(e.target.value)} required/>
                            <datalist id="dias">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                            </datalist>
                        </div>
                        <div className="input-box">
                            <label htmlFor="inicio">Início das Férias:</label>
                            <input id="inicio" type="date" name="inicio" value={dataInicio} onChange={e => setDataInicio(e.target.value)} required/>
                        </div>
                        <div className="input-box">
                            <label htmlFor="final">Final das Férias:</label>
                            <input id="final" name="final" value={dataDoFim(dataInicio, duracao).toLocaleDateString()} onChange={e => setDataFim(e.target.value)} disabled/>
                        </div>
                        <div className="input-box">
                            <label htmlFor="final">Comentário ao Gestor: (Opcional)</label>
                            <textarea row='7' id="comentario" type="text" name="comentario" placeholder="Deixe um comentário para seu gestor..." className='input-gestor' value={mensagemFuncionario} onChange={e => setMensagemFuncionario(e.target.value)} required/>
                        </div>
                    </div>
                    <div className="container-itens-treze">
                        <div className="">
                            <label>Deseja solicitar o adiantamento do 13º?</label>
                            <label htmlFor="adianta">Sim</label>
                            <input id="adiantamento" type="checkbox" name="adianta"required/>
                        </div>
                    
                    </div>
                    
                    <div className="box-botao">
                        <input type="submit" value="Solicitar" className="button" onClick={handleSubmit}/>
                    </div>            
                    <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal} style={customStyles}>
                    <div className='style-modal-solicitar'>
                        <p>Solicitação enviada!</p>
                    </div>                        
                    </Modal>  
                
                </fieldset>
            </div>
        </div>
    </div>
  )
}

export default FeriasGestor