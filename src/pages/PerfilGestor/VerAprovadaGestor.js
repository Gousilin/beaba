import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import './VerSolicitaGestor.css'
import Modal from 'react-modal'
import { useState, useEffect} from 'react'
import api from '../../services/api'
import { useParams } from 'react-router-dom'



const VerAprovadaGestor = () => {

    const {id} = useParams()

    const [solicitacao, setSolicitacao] = useState([])
    const [estrangeira, setEstrangeira] = useState([])

    useEffect(() => {
      async function loadSolicitacao(){
        const solicita = await api.get(`/listSolicitacao/${id}`)
        setSolicitacao(solicita.data)
        setEstrangeira(solicita.data.funcionario)
      }

      loadSolicitacao();

    }, [])


    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            justifyContent: 'center',
            alignText: 'center',
            width: '330px',
            borderRadius: '12px'
        }
    }

  return (
    <div>
    <HeaderGestor name="Painel do Gestor"/>
    <NavBarGestor/>
    <div className="bd-soli-gestor">
        <fieldset
        className="container-principal">
            <legend className="legenda-borda">Informações sobre a solicitação de férias</legend>
            <div className='status'>
                <p className='status-aprovada'>Aprovada</p>
            </div>
            <div className="container-itens">
                <div className="input-box">
                    <label htmlFor="nome">Nome Completo: </label>
                    <input id="nome" type="text" name="nome" placeholder={estrangeira.nome} disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="matricula">Matrícula:</label>
                    <input id="matricula" type="number" name="matricula" placeholder={solicitacao.idFuncionario} disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="data-solicita">Data de Solicitação:</label>
                    <input id="data-solicita" type="text" name="data-solicita" placeholder={new Date(solicitacao.data_solicitacao).toLocaleDateString('pt-br')} disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="dia">Quantidade de Dias:</label>
                    <input id="dia" type="text" name="dia" value={solicitacao.duracao} list="dias" disabled/>

                </div>
                <div className="input-box">
                    <label htmlFor="inicio">Início das Férias:</label>
                    <input id="inicio" type="text" name="inicio" placeholder={new Date(solicitacao.data_inicio).toLocaleDateString('pt-br')}disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="final">Final das Férias:</label>
                    <input id="final" type="text" name="final"  placeholder={new Date(solicitacao.data_final).toLocaleDateString('pt-br')}disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="final">Comentário para o Gestor:</label>
                    <textarea row='7' id="comentario" type="text" name="comentario" value={solicitacao.mensagem_funcionario}  className='input-gestor' disabled/>
                </div>
            </div>
            <div className="container-resposta">
                    <div className="itens-resposta">
                        <div className="input-box">
                        <label htmlFor="comentario" className='label-ajuste'>Comentário do Gestor:</label>
                        <textarea row='7' id="comentario" type="text" name="comentario" value={solicitacao.mensagem_gestor}  className='input-gestor' required/>
                        </div>
                    </div>

            </div>
        </fieldset>
    </div>
</div>
  )
}

export default VerAprovadaGestor