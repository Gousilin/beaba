import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import './VerSolicitaGestor.css'
import Modal from 'react-modal'
import { useState, useEffect} from 'react'
import api from '../../services/api'
import { Link, Navigate, redirect, useParams } from 'react-router-dom'



const VerSolicitaGestor = () => {
    const [modalIsOpen, setIsOpen] = useState(false)

    function handleOpenModal(){
        setIsOpen(true)
    }

    function handleCloseModal(){
        setIsOpen(false)
        window.location.href='/todas-solicitacoes'
    }

    const [modalIsOpenRecusa, setIsOpenRecusa] = useState(false)

    function handleOpenModalRecusa(){
        setIsOpenRecusa(true)
    }

    function handleCloseModalRecusa(){
        setIsOpenRecusa(false)
        window.location.href='/todas-solicitacoes'
    }

    const {id} = useParams()

    const [solicitacao, setSolicitacao] = useState([])
    const [estrangeira, setEstrangeira] = useState([])
    const [msgGestor, setMsgGestor] = useState('')

    useEffect(() => {
      async function loadSolicitacao(){
        const solicita = await api.get(`/listSolicitacao/${id}`)
        setSolicitacao(solicita.data)
        setEstrangeira(solicita.data.funcionario)
      }

      loadSolicitacao();

    }, [])


    async function handleAprovaGestor(){ 
            const resposta = {
                status: "Aprovada",
                mensagem_gestor: msgGestor
            }
            await api.put(`/updateSolicitacao/${id}`, resposta)   
            handleOpenModal()
    }

    async function handleRecusaGestor(){
            const resposta = {
                status: "Reprovada",
                mensagem_gestor: msgGestor
            } 
            handleOpenModalRecusa()
            await api.put(`/updateSolicitacao/${id}`, resposta)
    }





    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            justifyContent: 'center',
            alignText: 'center',
            width: '330px',
            borderRadius: '12px'
        }
    }

  return (
    <div>
    <HeaderGestor name="Painel do Gestor"/>
    <NavBarGestor/>
    <div className="bd-soli-gestor">
        <fieldset
        className="container-principal">
            <legend className="legenda-borda">Informações sobre a solicitação de férias</legend>
            <div className='status'>
                        <p className='status-analise'>Em análise</p>
            </div>
            <div className="container-itens">
                <div className="input-box">
                    <label htmlFor="nome">Nome Completo: </label>
                    <input id="nome" type="text" name="nome" placeholder={estrangeira.nome} disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="matricula">Matrícula:</label>
                    <input id="matricula" type="number" name="matricula" placeholder={solicitacao.idFuncionario} disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="data-solicita">Data de Solicitação:</label>
                    <input id="data-solicita" type="text" name="data-solicita" placeholder={new Date(solicitacao.data_solicitacao).toLocaleDateString('pt-br')} disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="dia">Quantidade de Dias:</label>
                    <input id="dia" type="text" name="dia" value={solicitacao.duracao} list="dias" disabled/>

                </div>
                <div className="input-box">
                    <label htmlFor="inicio">Início das Férias:</label>
                    <input id="inicio" type="text" name="inicio" placeholder={new Date(solicitacao.data_inicio).toLocaleDateString('pt-br')}disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="final">Final das Férias</label>
                    <input id="final" type="text" name="final"  placeholder={new Date(solicitacao.data_final).toLocaleDateString('pt-br')}disabled/>
                </div>
                <div className="input-box">
                    <label htmlFor="final">Comentário ao Gestor (Opcional)</label>
                    <textarea row='7' id="comentario" type="text" name="comentario" value={solicitacao.mensagem_funcionario}  className='input-gestor' disabled/>
                </div>
            </div>
            <div className="container-analise">
                <fieldset className="box-analise">
                    <legend className="legenda-borda">Informações sobre calendário atual</legend>
                    <div className="itens-analise">
                        <p>Férias agendadas de outros colaboradores para auxiliar sua decisão:</p><br></br>
                        <p>Ana Paula Nunes: 15/09/2023 - 30/09/2023</p>
                        <p>João Pedro Fernandes: 12/11/2023 - 22/11/2023</p>
                    </div>
                
                </fieldset>
                <div className="box-comentario">
                    <div className="itens-analise">
                        <div className="input-box">
                        <label htmlFor="comentario" className='label-ajuste'>Comentário (Opcional)</label>
                        <textarea row='7' id="comentario" type="text" name="comentario" placeholder="Escreva um comentário..." value={msgGestor} onChange={(e) => setMsgGestor(e.target.value)} className='input-gestor' required/>
                        </div>
                    </div>
                    <div className='box-botoes'>
                        <button className='botao-recusar' onClick={handleRecusaGestor}>Recusar</button>
                        <button className='botao-aprovar' onClick={handleAprovaGestor}>Aprovar</button>
                    </div>
                </div>
                <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal} style={customStyles}>
                    <div className='style-modal-aprova'>
                        <p>Férias aprovadas!</p>
                    </div>                        
                </Modal>
                <Modal isOpen={modalIsOpenRecusa} onRequestClose={handleCloseModalRecusa} style={customStyles}>
                    <div className='style-modal-recusa'>
                        <p>Férias recusadas!</p>
                    </div>                        
                </Modal>
            </div>
            
            
        </fieldset>
    </div>
</div>
  )
}

export default VerSolicitaGestor