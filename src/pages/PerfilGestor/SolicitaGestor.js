import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import { Link } from 'react-router-dom'
import './SolicitaGestor.css'
import { useState, useEffect} from 'react'
import api from '../../services/api'

const SolicitaGestor = () => {

    const [solicitacao, setSolicitacao] = useState([])
    const [aprovada, setAprovada] = useState([])
    const [reprovada, setReprovada] = useState([])

    useEffect(() => {
      async function loadSolicitacao(){
        const response = await api.get('/listSolicitacaoPendente')
        const aprove = await api.get('/listSolicitacaoA')
        const reprove = await api.get('/listSolicitacaoR')

        setSolicitacao(response.data)
        setAprovada(aprove.data)
        setReprovada(reprove.data)
      }

      loadSolicitacao();

    }, [])


  return (
    <div>
        <HeaderGestor name="Painel do Gestor"/>
        <NavBarGestor/>
        <div className="bd-soli-gestor">
            <div className='container-gestor'>
                <fieldset
                    className="container-principal">
                    <legend className="legenda-borda">Solicitação Pendente</legend>
                    {solicitacao.map((row) => (
                            <div className="container-list" key={row.id_solicitacao}>
                            <p>{row.funcionario.nome}</p>
                            <p>Período: {new Date(row.data_inicio).toLocaleDateString('pt-br')}---{new Date(row.data_final).toLocaleDateString('pt-br')}</p>
                            <p>Data Modificação: {new Date(row.updatedAt).toLocaleDateString('pt-br')}</p>
                            <Link to={`/todas-solicitacoes/ver-solicita/${row.id_solicitacao}`}  className='botao-visualizar'>Avaliar</Link>
                            </div>
                        ))}              
                
                </fieldset>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Solicitações Aprovadas/Reprovadas</legend>
                        {aprovada.map((row) => (
                                <div className="container-list aprovada" key={row.id_solicitacao}>
                                <p>{row.funcionario.nome}</p>
                                <p>Período: {new Date(row.data_inicio).toLocaleDateString('pt-br')}---{new Date(row.data_final).toLocaleDateString('pt-br')}</p>
                                <p>Data Modificação: {new Date(row.updatedAt).toLocaleDateString('pt-br')}</p>
                                <Link to={`/todas-solicitacoes/ver-solicita-aprovada/${row.id_solicitacao}`}  className='botao-visualizar'>Conferir</Link>
                                </div>
                            ))}    
                        {reprovada.map((row) => (
                                <div className="container-list reprovada" key={row.id_solicitacao}>
                                <p>{row.funcionario.nome}</p>
                                <p>Período: {new Date(row.data_inicio).toLocaleDateString('pt-br')}---{new Date(row.data_final).toLocaleDateString('pt-br')}</p>
                                <p>Data Modificação: {new Date(row.updatedAt).toLocaleDateString('pt-br')}</p>
                                <Link to={`/todas-solicitacoes/ver-solicita-reprovada/${row.id_solicitacao}`} className='botao-visualizar'>Conferir</Link>
                                </div>
                            ))}                 
                
                </fieldset>
            </div>
            
        </div>
    </div>
  )
}

export default SolicitaGestor