import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import NavBarLateral from '../../components/NavBarGestor/NavBarLateral'
import { Link } from 'react-router-dom'
import './MinhaSolicita.css'

const MinhaSolicita = () => {
  return (
    <div>
        <HeaderGestor name="Painel do Gestor"/>
        <NavBarGestor/>
        <body className="bd-my-gestor">
            <NavBarLateral/>
            <div className='container-my'>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Solicitação Pendente</legend>
                    <div className="container-list">
                        <p>Ana Maria Virginia</p>
                        <p>Período: 21/06/2023 - 20/07/2023</p>
                        <Link to="/verificar-solicitacao-gestor" className='botao-visualizar'>Visualizar</Link>
                    </div>                
                
                </fieldset>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Solicitações Aprovadas/Reprovadas</legend>
                        <div className="container-list aprovada">
                            <p>Ana Maria Virginia</p>
                            <p>Período: 21/06/2023 - 20/07/2023</p>
                            <Link to="/verificar-solicitacao-gestor" className='botao-visualizar'>Visualizar</Link>
                        </div>
                        <div className="container-list reprovada">
                            <p>Ana Maria Virginia</p>
                            <p>Período: 21/06/2023 - 20/07/2023</p>
                            <Link to="/verificar-solicitacao-gestor" className='botao-visualizar'>Visualizar</Link>
                        </div>               
                
                </fieldset>
            </div>
        </body>
    </div>
  )
}

export default MinhaSolicita