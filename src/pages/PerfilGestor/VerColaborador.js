import React from 'react'
import HeaderGestor from '../../components/HeaderGeral/HeaderGestor'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import { useState, useEffect} from 'react'
import api from '../../services/api'
import { Link, Navigate, redirect, useParams } from 'react-router-dom'

const VerColaborador = () => {

    const {id} = useParams()
    const [colaborador, setColaborador] = useState([])

    useEffect(() => {
      async function loadColaborador(){
        const colab = await api.get(`/listFuncionarios/${id}`)

        console.log(colab.data)
        setColaborador(colab.data)
      }

      loadColaborador();
    }, [])


  return (
    <div>
        <HeaderGestor name="Perfil do Gestor"/>
        <NavBarGestor/>
        <body className="body-cadastro">  
            <fieldset
            className="container-principal">
                <legend className="legenda-borda">Dados do Colaborador</legend>
                <div className="container-itens">
                    <div className="input-box">
                        <label for="nome">Nome Completo:</label>
                        <input id="nome" type="text" name="nome" placeholder={colaborador.nome} disabled/>
                    </div>
                    <div className="input-box">
                        <label for="data">Data de Nascimento:</label>
                        <input id="data" type="text" name="data" placeholder={new Date(colaborador.data_nasc).toLocaleDateString('pt-br')} disabled/>
                    </div>
                    <div className="input-box">
                        <label for="cpf">CPF:</label>
                        <input id="cpf" type="text" name="cpf" placeholder={colaborador.cpf} disabled/>
                    </div>
                    <div className="input-box">
                        <label for="email">e-mail:</label>
                        <input id="email" type="email" name="email" placeholder={colaborador.email_pessoal} disabled/>
                    </div>
                </div>
                
                <div className="container-itens">
                    <div className="input-box">
                        <label for="squad">Squad:</label>
                        <input id="squad" type="text" name="squad" placeholder={colaborador.idSquad} disabled/>
                    </div>
                    <div className="input-box">
                        <label for="cargo">Cargo:</label>
                        <input id="cargo" type="text" name="cargo" placeholder={colaborador.cargo} disabled/>
                    </div>
                    <div className="input-box">
                        <label for="contrato">Data de Contratação:</label>
                        <input id="contrato" type="text" name="contrato" placeholder={new Date(colaborador.data_contrato).toLocaleDateString('pt-br')} disabled/>
                    </div>
                    <div className="input-box">
                        <label for="matricula">Matrícula:</label>
                        <input id="matricula" type="number" name="matricula" placeholder={colaborador.id_matricula}  disabled/>
                    </div>
                    <div className="input-box">
                        <label for="regime">Regime:</label>
                        <input id="regime" type="text" name="regime" placeholder={colaborador.regime} disabled/>

                    </div>
                    <div className="input-box">
                        <label for="email-i">e-mail institucional:</label>
                        <input id="email-i" type="email-i" name="email-i" placeholder={colaborador.email_institucional}  disabled/>
                    </div>                    
                    
                </div>              
            </fieldset>
        </body>
    </div>
  )
}

export default VerColaborador