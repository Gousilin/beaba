import React from 'react'
import './LoginRh.css'
import logoQuero from '../../img/logo_quero.png'
import logoVerde from '../../img/logo_verde.jpg'
import {Link} from 'react-router-dom'
import { useState, useEffect } from 'react'
import api from '../../services/api'
import {login, logout, setIdFuncionario, setNomeFuncionario} from '../../services/auth'


export const LoginRh = () => {

    const [id_matricula, setIdMatricula] = useState('')
    const [senha_atual, setSenha] = useState('')
    const [ loading, setLoading ] = useState(false);
    
    async function handleSubmitLogin(){
        await api.post('/funcionarios/login', {id_matricula, senha_atual})
        .then(res => {
            if(res.status === 200){
                if(res.data.status===1){
                    login(res.data.token)
                    setIdFuncionario(res.data.id_matricula)
                    setNomeFuncionario(res.data.nome)

                    window.location.href='/meu-panorama'

                } else if (res.data.status === 2){
                    alert('Atenção, algo não está certo ')
                }
                setLoading(false);
            } else{
                alert('Erro no servidor')
                setLoading(false);
            }
        })
    }

  return (
    <div className='login-body'>
        <div className='box-verde-logo'>
            <img src={logoVerde} alt="Verde-card" className="verde-logo"/>
        </div>
        <div className='container-geral'>
            <fieldset className="container-login">
                <legend className="borda-container-login"><img src={logoQuero} alt="Quero-Quero" className="quero-logo"/></legend>
                <div className='caixa-dados'>
                    <h2>Cadastro de Funcionários</h2>
                    <div className="input-login">
                        <label htmlFor="login" value={senha_atual} onChange={(e) => setSenha(e.target.value)} >Login:</label>
                        <input id="login" type="text" name="login" placeholder="Digite seu login..." required/>
                    </div>
                    <div className="input-login">
                        <label htmlFor="data" value={senha_atual} onChange={(e) => setSenha(e.target.value)} >Senha:</label>
                        <input id="password" type="password" name="password" placeholder="Digite sua senha..." required/>
                    </div>
                    <div className="botoes-login">
                        <button className='texto-botao' onClick={handleSubmitLogin} >Entrar</button>
                    </div>
                    <div className="botoes-login">
                        <Link to="/" className='texto-redefinir'>Login exclusivo para RH. Deseja retornar?</Link>
                    </div>

                </div>
            </fieldset>
        </div> 


    </div>
  )
}

export default LoginRh;