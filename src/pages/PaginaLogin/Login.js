import React from 'react'
import './Login.css'
import logoQuero from '../../img/logo_quero.png'
import logoVerde from '../../img/logo_verde.jpg'
import {Link, Navigate} from 'react-router-dom'
import { useState, useEffect } from 'react'
import api from '../../services/api'
import {login, logout, setIdFuncionario, setNomeFuncionario, setCargoFuncionario} from '../../services/auth'

export const Login = () => {

    const [id_matricula, setIdMatricula] = useState('')
    const [senha_atual, setSenha] = useState('')
    const [ loading, setLoading ] = useState(false);
    
    async function handleSubmitLogin(){
        await api.post('/funcionarios/login', {id_matricula, senha_atual})
        .then(res => {
            if(res.status === 200){

                if(res.data.status===1){
                    login(res.data.token)
                    setIdFuncionario(res.data.id_matricula)
                    setNomeFuncionario(res.data.nome)
                    setCargoFuncionario(res.data.cargo)

                    window.location.href='/dashboard-gestor'


                } else if (res.data.status === 2){
                    alert('Atenção, algo não está certo ')
                }
                setLoading(false);
            } else{
                alert('Erro no servidor')
                setLoading(false);
            }
        })
    }


  return (
    <div className='login-body'>
        <div className='box-verde-logo'>
            <img src={logoVerde} alt="Verde-card" className="verde-logo"/>
        </div>
        <div className='container-geral'>
            <fieldset className="container-login">
                <legend className="borda-container-login"><img src={logoQuero} alt="Quero-Quero" className="quero-logo"/></legend>
                <div className='caixa-dados'>
                    <h2>Quero-Quero Férias</h2>
                    <div className="input-login">
                        <label htmlFor="login">Login:</label>
                        <input id="login" type="text" name="login" placeholder="Digite seu login..." value={id_matricula} onChange={(e) => setIdMatricula(e.target.value)} required/>
                    </div>
                    <div className="input-login">
                        <label htmlFor="data">Senha:</label>
                        <input id="password" type="password" name="password" placeholder="Digite sua senha..." value={senha_atual} onChange={(e) => setSenha(e.target.value)} required/>
                    </div>
                    <div className="botoes-login">
                        <button><Link to="/" className='texto-botao'>Voltar</Link></button>
                        <button className='texto-botao'onClick={handleSubmitLogin} >Entrar</button>
                    </div>
                    <div className="botoes-login">
                        <Link to="/esqueci-senha" className='texto-redefinir'>Esqueci minha senha</Link>
                    </div>

                </div>
            </fieldset>
        </div> 


    </div>
  )
}

export default Login;