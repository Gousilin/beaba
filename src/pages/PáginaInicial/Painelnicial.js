import React from 'react'
import './PainelInicial.css'
import logoQuero from '../../img/logo_quero.png'
import logoVerde from '../../img/logo_verde.jpg'
import {Link, Navigate} from 'react-router-dom'

export const PainelInicial = () => {

  return (
    <div className='login-body'>
        <div className='box-verde-logo'>
            <img src={logoVerde} alt="Verde-card" className="verde-logo"/>
        </div>
        <div className='container-geral-entrada'>
            <fieldset className="container-entrada">
                <legend className="borda-container-login"><img src={logoQuero} alt="Quero-Quero" className="quero-logo"/></legend>
                <div className='caixa-dados-entrada'>
                    <h2>Quero-Quero Férias</h2>
                    <div className="botoes-entrada">
                        <button><Link to="/login-rh" className='texto-botao-entrada'>Cadastro</Link></button>
                        <button><Link to="/login-colaborador" className='texto-botao-entrada'>Colaborador</Link></button>
                        <button><Link to="/login-gestor" className='texto-botao-entrada'>Gestor</Link></button>
                    </div>


                </div>
            </fieldset>
        </div> 


    </div>
  )
}

export default PainelInicial;