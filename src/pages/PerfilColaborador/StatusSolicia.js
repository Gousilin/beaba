import React from 'react'
import NavBarColab from '../../components/NavBarColab/NavBarColab'
import { Link } from 'react-router-dom'
import { getIdFuncionario } from '../../services/auth'
import { useState, useEffect } from 'react'
import api from '../../services/api'

import HeaderGeral from '../../components/HeaderGeral/HeaderGeral'
import './StatusSolicita.css'

const StatusSolicia = () => {

    const [funcionarioSolicita, setFuncionarioSolicita] = useState([])

    const id = window.localStorage.getItem('&id_matricula')

    useEffect(() => {
      async function loadFuncionarioSolicita(){
        const responseSolicita = await api.get(`/listSolicitacao`)

        function getMyData(){
            if (responseSolicita.idFuncionario === id){
                setFuncionarioSolicita(responseSolicita.data)
                console.log(responseSolicita.data)
            }
        }

        getMyData();

      }

      
      loadFuncionarioSolicita();

    }, [])

    
  return (
    <div>
        <HeaderGeral name="Painel do Colaborador"/>
        <NavBarColab/>
        <div className="bd-soli-gestor">
            <div className='container-gestor'>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Solicitação Pendente</legend>
                    {funcionarioSolicita.map((row) => (
                            <div className="container-list" key={row.id_solicitacao}>
                            <p>{row.nome}</p>
                            <p>Período: {new Date(row.data_inicio).toLocaleDateString('pt-br')}---{new Date(row.data_final).toLocaleDateString('pt-br')}</p>
                            <p>Data Modificação: {new Date(row.updatedAt).toLocaleDateString('pt-br')}</p>
                            <Link to={`/todas-solicitacoes/ver-solicita/${row.id_solicitacao}`}  className='botao-visualizar'>Avaliar</Link>
                            </div>
                        ))}
                
                </fieldset>
                <fieldset
                className="container-principal">
                    <legend className="legenda-borda">Solicitações Aprovadas/Reprovadas</legend>
                        {funcionarioSolicita.map((row) => (
                            <div className="container-list" key={row.id_solicitacao}>
                            <p>{row.funcionario.nome}</p>
                            <p>Período: {new Date(row.data_inicio).toLocaleDateString('pt-br')}---{new Date(row.data_final).toLocaleDateString('pt-br')}</p>
                            <p>Data Modificação: {new Date(row.updatedAt).toLocaleDateString('pt-br')}</p>
                            <Link to={`/todas-solicitacoes/ver-solicita-aprovada/${row.id_solicitacao}`}  className='botao-visualizar'>Avaliar</Link>
                            </div>
                        ))}
                        {funcionarioSolicita.map((row) => (
                            <div className="container-list" key={row.id_solicitacao}>
                            <p>{row.funcionario.nome}</p>
                            <p>Período: {new Date(row.data_inicio).toLocaleDateString('pt-br')}---{new Date(row.data_final).toLocaleDateString('pt-br')}</p>
                            <p>Data Modificação: {new Date(row.updatedAt).toLocaleDateString('pt-br')}</p>
                            <Link to={`/todas-solicitacoes/ver-solicita-reprovada/${row.id_solicitacao}`}  className='botao-visualizar'>Avaliar</Link>
                            </div>
                        ))}
           
                
                </fieldset>
            </div>
        </div>
    </div>
  )
}

export default StatusSolicia;