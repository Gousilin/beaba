import React from 'react'
import NavBarColab from '../../components/NavBarColab/NavBarColab'
import HeaderGeral from '../../components/HeaderGeral/HeaderGeral'
import './VerificarSolicita.css'

const VerificarSolicita = () => {
  return (
    <div>
    <HeaderGeral name="Painel do Colaborador"/>
    <NavBarColab/>
    <body className="body-cadastro">
        
        <fieldset
        className="container-principal">
            <legend className="legenda-borda">Status Solicitação</legend>
            <div className='status'>
                <p className='status-analise'>Em análise</p>
            </div>
            <div className="container-itens">
                <div className="input-box">
                    <label for="nome">Nome Completo:</label>
                    <input id="nome" type="text" name="nome" placeholder="Digite seu nome completo" disabled/>
                </div>
                <div className="input-box">
                    <label for="matricula">Matrícula:</label>
                    <input id="matricula" type="number" name="matricula" placeholder="Informe a matrícula" disabled/>
                </div>
                <div className="input-box">
                    <label for="data-solicita">Data de Solicitação:</label>
                    <input id="data-solicita" type="date" name="data-solicita"disabled/>
                </div>
                <div className="input-box">
                    <label for="dia">Quantidade de Dias:</label>
                    <input id="dia" type="text" name="dia" placeholder="Selecione os dias" list="dias" disabled/>
                    <datalist id="dias">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                    </datalist>
                </div>
                <div className="input-box">
                    <label for="inicio">Início das Férias:</label>
                    <input id="inicio" type="date" name="inicio" disabled/>
                </div>
                <div className="input-box">
                    <label for="final">Final das Férias</label>
                    <input id="final" type="date" name="final" disabled/>
                </div>
                <div className="input-box">
                    <label for="final">Comentário ao Gestor (Opcional)</label>
                    <textarea row='7' id="comentario" type="text" name="comentario" placeholder="Deixe um comentário para seu gestor..." className='input-gestor' disabled/>
                </div>
            </div>
            <div className="container-itens-treze">
                    <div className="box-treze">
                    <label>Deseja solicitar o adiantamento do 13º?</label>
                        <label for="adianta">Sim</label>
                    </div>
                
            </div>
            
            
        </fieldset>
    </body>
</div>
  )
}

export default VerificarSolicita