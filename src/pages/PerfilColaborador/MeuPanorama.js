import React from 'react'
import HeaderGeral from '../../components/HeaderGeral/HeaderGeral'
import NavBarColab from '../../components/NavBarColab/NavBarColab'
import './MeuPanorama.css'
import Calendar from 'react-calendar'
import '../../components/Calendario/Calendario.css'

const MeuPanorama = () => {


  return (
    <div>
        <HeaderGeral name="Painel do Colaborador"/>
        <NavBarColab/>
        <div className="body-panorama">
            
            <fieldset
            className="container-principal-panorama">
                <legend className="legenda-borda">Meu Panorama</legend>
                <div className="container-panorama">

                    <Calendar selectRange={true} value={[new Date('2023-04-02'), new Date('2023-04-04')]}/>
                </div>
                <div className="container-panoinfo">
                    <h2>Informações</h2><hr/>
                    <div className="container-info">
                        <p>Suas férias estão agendadas para 19/05/23!</p>

                    </div>
                    <div className="container-info">
                        <p>Você já solicitou o adiantamento do 13º!</p>

                    </div>

                </div>
                                 
            
            </fieldset>
        </div>
    </div>
  )
}

export default MeuPanorama