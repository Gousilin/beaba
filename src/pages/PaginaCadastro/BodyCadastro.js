import React from 'react'
import './BodyCadastro.css'
import Header from '../../components/Header-Cadastro/Header'
import Modal from 'react-modal'
import { useState, useEffect } from 'react'
import api from '../../services/api'

Modal.setAppElement('#root')

const BodyCadastro = () => {
    const [modalIsOpen, setIsOpen] = useState(false)
    const [idFuncionario, setIdFuncionario] = useState('')
    const [nome, setNome] = useState('')
    const [cpf, setCPF] = useState('')
    const [dataNasc, setDataNasc] = useState('')
    const [dataContrato, setDataContrato] = useState('')
    const [regime, setRegime] = useState('')
    const [senhaPadrao, setSenhaPadrao] = useState('')
    const [email, setEmail] = useState('')
    const [emailQQ, setEmailQQ] = useState('')
    const [squad, setSquad] = useState('')
    const [gestor, setGestor] = useState('')
    const [cargo, setCargo] = useState('')

    async function handleSubmit(){

        const data = {
            id_matricula: idFuncionario, 
            nome: nome, 
            cpf: cpf, 
            data_nasc: dataNasc, 
            data_contrato: dataContrato, 
            regime: regime, 
            senha_padrao: senhaPadrao,
            senha_atual: senhaPadrao, 
            email_pessoal: email, 
            email_institucional: emailQQ, 
            idSquad: squad, 
            idGestor: gestor, 
            cargo: cargo
        }

        if (data.id_matricula === "" || data.nome === "" || data.data_nasc === "" || data.data_contrato === "" || data.regime === "" || data.senha_padrao === "" || data.email_pessoal === "" || data.email_institucional === "" || data.idSquad === "" || data.idGestor === "" || data.cargo === ""){
            alert("Por favor, preencha todos os campos!")
        } else{
            const response = await api.post('/saveFuncionarios', data)
        
            console.log(data)
            console.log(response)

            handleOpenModal()
        }

        
    }

    const [gestores, setGestores] = useState([])
    const [squads, setSquads] = useState([])

    useEffect(() => {
      async function loadGestores(){
        const respGestores = await api.get('/listFuncionariosGestores')

        console.log(respGestores.data)
        setGestores(respGestores.data)
      }
      loadGestores();
    }, [])

    useEffect(() => { 
        async function loadSquads(){
        const respSquads = await api.get('/listSquad')

        console.log(respSquads.data)
        setSquads(respSquads.data)
      }

      loadSquads()
    }, [])


    //modals

    function handleOpenModal(){
        setIsOpen(true)
    }
    
    function handleCloseModal(){
        setIsOpen(false)
    }

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            width: '350px',
            borderRadius: '12px'
        }
    }



  return (
    <div>
        <Header/>
        <body className="body-cadastro">
            
            <fieldset
            className="container-principal">
                <legend className="legenda-borda">Preencha o formulário a baixo para cadastrar funcionários</legend>
                <div className="container-itens">
                    <div className="input-box">
                        <label for="nome">Nome Completo:</label>
                        <input id="nome" type="text" name="nome" placeholder="Digite seu nome completo" value={nome} onChange={e => setNome(e.target.value)} required/>
                    </div>
                    <div className="input-box">
                        <label for="data">Data de Nascimento:</label>
                        <input id="data" type="date" name="data" required value={dataNasc} onChange={e => setDataNasc(e.target.value)}/>
                    </div>
                    <div className="input-box">
                        <label for="cpf">CPF:</label>
                        <input id="cpf" type="text" name="cpf" placeholder="___.___.___-__" value={cpf} onChange={e => setCPF(e.target.value)} required/>
                    </div>
                    <div className="input-box">
                        <label for="email">e-mail:</label>
                        <input id="email" type="email" name="email" placeholder="example@example.com" value={email} onChange={e => setEmail(e.target.value)} required/>
                    </div>
                </div>
                
                <div className="container-itens">
                    <div className="input-box">
                        <label for="squad">Squad:</label>
                        <input id="squad" type="text" name="squad" placeholder="Selecione a squad" list="squads" value={squad} onChange={e => setSquad(e.target.value)} required/>
                        <datalist id="squads">
                                {squads.map((row) => 
                                    <div key={row._id}>
                                        <option value={row.id_squad}>{row.nome_squad}</option>
                                    </div>
                                )}
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="cargo">Cargo:</label>
                        <input id="cargo" type="text" name="cargo" placeholder="Selecione o cargo" list="cargos" value={cargo} onChange={e => setCargo(e.target.value)} required/>
                        <datalist id="cargos">
                            <option>Gestor</option>
                            <option>Colaborador</option>
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="cargo">Gestor:</label>
                        <input id="gestor" type="text" name="gestor" placeholder="Selecione o gestor" list="gestores" value={gestor} onChange={e => setGestor(e.target.value)} required/>
                        <datalist id="gestores">
                                {gestores.map((row) => 
                                    <div key={row._id}>
                                        <option value={row.id_matricula}>{row.nome}</option>
                                    </div>
                                )}
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="contrato">Data de Contratação:</label>
                        <input id="contrato" type="date" name="contrato" value={dataContrato} onChange={e => setDataContrato(e.target.value)} required/>
                    </div>
                    <div className="input-box">
                        <label for="matricula">Matrícula:</label>
                        <input id="matricula" type="number" name="matricula" placeholder="Informe a matrícula" value={idFuncionario} onChange={e => setIdFuncionario(e.target.value)} required/>
                    </div>
                    <div className="input-box">
                        <label for="regime">Regime:</label>
                        <input id="regime" type="text" name="regime" placeholder="Selecione o cargo" list="regimes" value={regime} onChange={e => setRegime(e.target.value)} required/>
                        <datalist id="regimes">
                            <option>CLT</option>
                            <option>PJ</option>
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="email-i">e-mail institucional:</label>
                        <input id="email-i" type="email-i" name="email-i" placeholder="example@example.com" value={emailQQ} onChange={e => setEmailQQ(e.target.value)} required/>
                    </div>
                    <div className="input-box">
                        <label for="senha-p">Senha Padrão:</label>
                        <input id="senha-p" type="password" name="senha-p" placeholder="Defina a senha padrão" required/>
                    </div>
                    <div className="input-box">
                        <label for="contirma-senha-p">Confirme a Senha:</label>
                        <input id="contirma-senha-p" type="password" name="contirma-senha-p" placeholder="Confirme a senha padrão" value={senhaPadrao} onChange={e => setSenhaPadrao(e.target.value)} required/>
                    </div>
                    
                </div>
                <div className="box-botao">
                    <input type="submit" value="Cadastrar" className="button" onClick={handleSubmit}/>
                </div>
                <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal} style={customStyles}>
                        <div className='style-modal-cadastro'>
                                <p>Cadastro realizado com sucesso!</p>
                        </div>
                        
                </Modal>
                
            
            </fieldset>
        </body>
    </div>
  );
};

export default BodyCadastro;