import React from 'react'
import Header from '../../components/HeaderGeral/HeaderGeral'
import NavBarGestor from '../../components/NavBarGestor/NavBarGestor'
import Modal from 'react-modal'
import { useState } from 'react'

Modal.setAppElement('#root')

const MeuPerfilGestor = () => {

    const [modalIsOpen, setIsOpen] = useState(false)

    function handleOpenModal(){
        setIsOpen(true)
    }

    function handleCloseModal(){
        setIsOpen(false)
    }


    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            width: '280px',
            borderRadius: '12px'
        }
    }

  return (
    <div>
        <Header name="Perfil do Gestor"/>
        <NavBarGestor/>
        <body className="body-cadastro">  
            <fieldset
            className="container-principal">
                <legend className="legenda-borda">Meus Dados</legend>
                <div className="container-itens">
                    <div className="input-box">
                        <label for="nome">Nome Completo:</label>
                        <input id="nome" type="text" name="nome" placeholder="Pedro Pedroso Pereira" disabled/>
                    </div>
                    <div className="input-box">
                        <label for="data">Data de Nascimento:</label>
                        <input id="data" type="date" name="data" disabled/>
                    </div>
                    <div className="input-box">
                        <label for="cpf">CPF:</label>
                        <input id="cpf" type="text" name="cpf" placeholder="3452673984-23" disabled/>
                    </div>
                    <div className="input-box">
                        <label for="email">e-mail:</label>
                        <input id="email" type="email" name="email" placeholder="example@example.com" disabled/>
                    </div>
                </div>
                
                <div className="container-itens">
                    <div className="input-box">
                        <label for="squad">Squad:</label>
                        <input id="squad" type="text" name="squad" placeholder="Dev Web" list="squads" disabled/>
                        <datalist id="squads">
                            <option>Phyton</option>
                            <option>UX</option>
                            <option>Dev Web</option>
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="cargo">Cargo:</label>
                        <input id="cargo" type="text" name="cargo" placeholder="Colaborador" list="cargos" disabled/>
                        <datalist id="cargos">
                            <option>Gestor</option>
                            <option>Colabrador</option>
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="contrato">Data de Contratação:</label>
                        <input id="contrato" type="date" name="contrato"disabled/>
                    </div>
                    <div className="input-box">
                        <label for="matricula">Matrícula:</label>
                        <input id="matricula" type="number" name="matricula" placeholder="234512" disabled/>
                    </div>
                    <div className="input-box">
                        <label for="regime">Regime:</label>
                        <input id="regime" type="text" name="regime" placeholder="CLT" list="regimes" disabled/>
                        <datalist id="regimes">
                            <option>CLT</option>
                            <option>PJ</option>
                        </datalist>
                    </div>
                    <div className="input-box">
                        <label for="email-i">e-mail institucional:</label>
                        <input id="email-i" type="email-i" name="email-i" placeholder="example@example.com" disabled/>
                    </div>
                    <div className="input-box">
                        <label for="senha-p">Alterar Senha:</label>
                        <input id="senha-p" type="password" name="senha-p" placeholder="xxxxxxxxxxx" required/>
                    </div>
                    <div className="input-box">
                        <label for="senha-p">Senha Atual:</label>
                        <input id="senha-p" type="password" name="senha-p" placeholder="xxxxxxxxxxx" required/>
                    </div>
                    
                    
                </div>              
                <div className="box-botao">
                    <input type="submit" value="Alterar" className="button" onClick={handleOpenModal}/>
                </div>
                <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal} style={customStyles}>
                    <div className='style-modal-solicitar'>
                        <p>Senha alterada!</p>
                    </div>                        
                </Modal>  
            </fieldset>
        </body>
    </div>
  )
}

export default MeuPerfilGestor