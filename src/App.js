import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import BodyCadastro from './pages/PaginaCadastro/BodyCadastro';
import Login from './pages/PaginaLogin/Login';
import EsqueciSenha from './pages/EsqueciSenha/EsqueciSenha';
import MeuPanorama from './pages/PerfilColaborador/MeuPanorama';
import StatusSolicia from './pages/PerfilColaborador/StatusSolicia';
import SolicitaFerias from './pages/PerfilColaborador/SolicitaFerias';
import Colaboradores from './pages/PerfilGestor/Colaboradores';
import SolicitaGestor from './pages/PerfilGestor/SolicitaGestor';
import FeriasGestor from './pages/PerfilGestor/FeriasGestor';
import MinhaSolicita from './pages/PerfilGestor/MinhaSolicita';
import Dashboard from './pages/PerfilGestor/Dashboard';
import MeuPerfil from './pages/MeuPerfil/MeuPerfil';
import MeuPerfilGestor from './pages/MeuPerfil/MeuPerfilGestor';
import VerificarSolicita from './pages/PerfilColaborador/VerificarSolicita';
import VerStatusGestor from './pages/PerfilGestor/VerStatusGestor';
import VerColaborador from './pages/PerfilGestor/VerColaborador';
import VerSolicitaGestor from './pages/PerfilGestor/VerSolicitaGestor';
import LoginRh from './pages/PaginaLoginRh/LoginRh';
import LoginColab from './pages/PaginaLogin/LoginColab';
import PainelInicial from './pages/PáginaInicial/Painelnicial';
import VerAprovadaGestor from './pages/PerfilGestor/VerAprovadaGestor';
import VerReprovadaGestor from './pages/PerfilGestor/VerReprovadaGestor';
import PrivateRoute from './services/wAuth'
import PrivateRouteF from './services/wAuthF'

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<PainelInicial/>}></Route>
        <Route path="/esqueci-senha" element={<EsqueciSenha/>}></Route>

        {/*Cadastro*/}
        <Route path="/login-rh" element={<LoginRh/>}></Route>
        <Route path="/cadastro" exact  element= {<PrivateRoute><BodyCadastro/></PrivateRoute>}></Route>

        {/*Colaborador*/}
        <Route path="/login-colaborador" element={<LoginColab/>}></Route>
        <Route path="/meu-panorama" exact element={<PrivateRouteF><MeuPanorama/></PrivateRouteF>}></Route>
        <Route path="/status-colab" exact element={<PrivateRouteF><StatusSolicia/></PrivateRouteF>}></Route>
        <Route path="/solicitacao-colab" exact element={<PrivateRouteF><SolicitaFerias/></PrivateRouteF>}></Route>
        <Route path="/meu-perfil/:id" exact element={<PrivateRouteF><MeuPerfil/></PrivateRouteF>}></Route>
        <Route path="/verificar-solicitacao/:id" exact element={<PrivateRouteF><VerificarSolicita/></PrivateRouteF>}></Route>

        {/*Gestor*/}
        <Route path="/login-gestor" element={<Login/>}></Route>
        <Route path="/pesquisa-colaboradores" exact element={<PrivateRoute><Colaboradores/></PrivateRoute>}></Route>
        <Route path="/todas-solicitacoes" exact element={<PrivateRoute><SolicitaGestor/></PrivateRoute>}></Route>
        <Route path="/minhas-gestor/solicitacao" exact element={<PrivateRoute><FeriasGestor/></PrivateRoute>}></Route>
        <Route path="/minhas-gestor/status" exact element={<PrivateRoute><MinhaSolicita/></PrivateRoute>}></Route>
        <Route path="/dashboard-gestor" exact element={<PrivateRoute><Dashboard/></PrivateRoute>}></Route>
        <Route path="/meu-perfil-gestor/:id" exact element={<PrivateRoute><MeuPerfilGestor/></PrivateRoute>}></Route>
        <Route path="/verificar-solicitacao-gestor" exact element={<PrivateRoute><VerStatusGestor/></PrivateRoute>}></Route>
        <Route path='/pesquisa-colaboradores/colaborador/:id' exact element={<PrivateRoute><VerColaborador/></PrivateRoute>}></Route>
        <Route path='/todas-solicitacoes/ver-solicita/:id' element={<PrivateRoute><VerSolicitaGestor/></PrivateRoute>}></Route>
        <Route path='/todas-solicitacoes/ver-solicita-aprovada/:id' element={<PrivateRoute><VerAprovadaGestor/></PrivateRoute>}></Route>
        <Route path='/todas-solicitacoes/ver-solicita-reprovada/:id' element={<PrivateRoute><VerReprovadaGestor/></PrivateRoute>}></Route>

      </Routes>
    </Router>
    
  );
}

export default App;
