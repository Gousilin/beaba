import React, { useEffect, useState } from 'react';
import api from './api';
import { login, logout, getToken } from './auth';
import { Route, Navigate } from 'react-router-dom';


export default function WAuth ({ children, component: Component, ...rest }){
    const [ redirect, setRedirect ] = useState(true);
    const [ loading, setLoading ] = useState(false);

    useEffect(() => {
        async function verify(){
            var res = await api.get('/funcionarios/checktoken', {params:{token:getToken()}});
            console.log(res)
            if(res.data.status===200){
                setLoading(false);
                setRedirect(false);
                

            }else{
                logout();
                setLoading(false);
                setRedirect(true);
            }
        }

        verify();
    },[])

    return (
        loading?'Carregando ...': !redirect? (children):
             <Navigate to={{pathname: "/dashboard-gestor"}} />      
    )

 }
    
