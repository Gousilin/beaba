export const TOKEN_KEY = '&app-token';
export const ID_FUNCIONARIO = '&id_matricula';
export const NOME_FUNCIONARIO = '&nome';
export const USER_TYPE = '&cargo';

export const login = token => {
    localStorage.setItem(TOKEN_KEY, token)
}

export const loginColab = token => {
    localStorage.setItem(TOKEN_KEY, token)
}

export const logout = () => {
    localStorage.clear()
}

export const setIdFuncionario = id_matricula => {localStorage.setItem(ID_FUNCIONARIO, id_matricula)}
export let getIdFuncionario = () => {localStorage.getItem(ID_FUNCIONARIO)}

export const setNomeFuncionario = nome => {localStorage.setItem(NOME_FUNCIONARIO, nome)}
export let getNomeFuncionario = () => {localStorage.getItem(NOME_FUNCIONARIO)}

export const setCargoFuncionario = cargo => {localStorage.setItem(USER_TYPE, cargo)}
export let getCargoFuncionario = () => {localStorage.getItem(USER_TYPE)}


export const getToken = () => localStorage.getItem(TOKEN_KEY)